//
//  AppDelegate.swift
//  Notifications
//
//  Created by Nikita Marchenko on 9/23/19.
//  Copyright © 2019 Nikita Marchenko. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        googleServicesSetup()

        return true
    }

    private func googleServicesSetup() {
        Messaging.messaging().delegate = self
        FirebaseApp.configure()
    }
}

// MARK: - UNUserNotificationCenterDelegate, MessagingDelegate
extension AppDelegate: UNUserNotificationCenterDelegate, MessagingDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case "accept_action":
            print("Accept was selected")
        case "delete_action":
            print("Delete was selected")
        default:
            print("Nothing was selected")
        }
    }
}
