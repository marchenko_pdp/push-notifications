//
//  NotificationsService.swift
//  Notifications
//
//  Created by Nikita Marchenko on 5/5/20.
//  Copyright © 2020 Nikita Marchenko. All rights reserved.
//

import UserNotifications

final class NotificationsService {
    
    static var shared = NotificationsService()
    
    func checkStatus() {
        let current = UNUserNotificationCenter.current()
        current.getNotificationSettings { settings in
            switch settings.authorizationStatus {
            case .authorized:
                DispatchQueue.main.async {
                    NotificationCenter.default.post(name: .notificationsHasBeenAuthorized,
                    object: nil)
                }
            default: break
            }
        }
    }
    
    func askForNotificationPermissions() {
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        center.requestAuthorization(options: options) { isAuthorized, error in
            guard error == nil && isAuthorized else { return }
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: .notificationsHasBeenAuthorized,
                object: nil)
            }
            debugPrint("Notifications has been authorized")
        }
    }
    
    func requestNotification(with props: Props, trigger: UNNotificationTrigger?) {
        let notification = notificationContent(for: props)
        
        let request = UNNotificationRequest(identifier: props.id,
                                            content: notification,
                                            trigger: trigger)
        
        UNUserNotificationCenter.current().add(request) { _ in }
    }
    
    func requestNotification(with props: Props, actions: [UNNotificationAction]) {
        let notificationCenter = UNUserNotificationCenter.current()
        let notification = notificationContent(for: props)
        let categoryIdentifier = "category_identifier"
        
        notification.categoryIdentifier = categoryIdentifier
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5,
                                                        repeats: false)
        let request = UNNotificationRequest(identifier: props.id,
                                            content: notification,
                                            trigger: trigger)
        
        notificationCenter.add(request) { _ in }
        
        let category = UNNotificationCategory(identifier: categoryIdentifier,
                                              actions: actions,
                                              intentIdentifiers: [], options: .customDismissAction)
        
        notificationCenter.setNotificationCategories([category])
    }
    
    private func notificationContent(for props: Props) -> UNMutableNotificationContent {
        let notification = UNMutableNotificationContent()
        notification.title = props.title
        notification.body = props.body

        return notification
    }
    
    func remove(identifier: String?) {
        guard let identifier = identifier else { return }
        UNUserNotificationCenter
            .current()
            .removeDeliveredNotifications(withIdentifiers: [identifier])
    }

    func removeAll() {
        UNUserNotificationCenter
            .current()
            .removeAllPendingNotificationRequests()
    }
}

extension NotificationsService {
    struct Props {
        let id: String
        let title: String
        let body: String
    }
}
