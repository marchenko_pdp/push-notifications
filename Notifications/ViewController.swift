//
//  ViewController.swift
//  Notifications
//
//  Created by Nikita Marchenko on 9/23/19.
//  Copyright © 2019 Nikita Marchenko. All rights reserved.
//

import UIKit
import NotificationCenter

class ViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet private weak var grouppedButton: UIButton!
    @IBOutlet private weak var categorybutton: UIButton!
    @IBOutlet private weak var permissionsButton: UIButton!
    
    // MARK: - Dependencies
    private let notificationsService = NotificationsService.shared
    let notificationCenter = NotificationCenter.default
    private var isNotificactionsAllowed = false {
        didSet {
            setupButtons()
        }
    }

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupButtons()
        
        notificationsService.checkStatus()
        notificationCenter.addObserver(self,
                                       selector: #selector(notificationsAuthorized),
                                       name: .notificationsHasBeenAuthorized,
                                       object: nil)
    }
    
    @objc
    private func notificationsAuthorized() {
        isNotificactionsAllowed = true
    }
    
    private func setupButtons() {
        let buttonsColor: UIColor = isNotificactionsAllowed ?
            .systemBlue :
            .systemGray
        
        permissionsButton.backgroundColor = isNotificactionsAllowed ?
            .systemGreen :
            .systemBlue
        grouppedButton.backgroundColor = buttonsColor
        categorybutton.backgroundColor = buttonsColor
    }

    func addGroupprdNotifications() {
        let saveAction = UNNotificationAction(identifier: "accept_action",
                                              title: "Accept",
                                              options: .foreground)
        let deleteAction = UNNotificationAction(identifier: "delete_action",
                                                title: "Delete",
                                                options: .foreground)
        let notificationsCategory =
            UNNotificationCategory(identifier: "notifications_category",
                                   actions: [saveAction, deleteAction],
                                   intentIdentifiers: [],
                                   hiddenPreviewsBodyPlaceholder: "",
                                   options: .customDismissAction)

        for number in 1...5 {
            let notificationContent = number & 2 == 0
                ? makeTeaNotification()
                : makeWorkNotification()

            let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 5,
                                                            repeats: false)

            let request = UNNotificationRequest(identifier: "\(number)FiveSeconds",
                content: notificationContent,
                trigger: trigger)
            let center = UNUserNotificationCenter.current()
            center.setNotificationCategories([notificationsCategory])
            center.add(request) { error in
                if let error = error { print(error) }
            }
        }
    }

    func makeTeaNotification() -> UNMutableNotificationContent {
        let notificationContent = UNMutableNotificationContent()

        notificationContent.title = "Kitchen"
        notificationContent.body = "Don't forget the tea"
        notificationContent.threadIdentifier = "Tea-Identifier"
        notificationContent.summaryArgument = "your cup of tea"

        return notificationContent
    }

    func makeWorkNotification() -> UNMutableNotificationContent {
        let notificationContent = UNMutableNotificationContent()

        notificationContent.title = "Green Office"
        notificationContent.body = "I invite you to meeting"
        notificationContent.threadIdentifier = "Office-Identifier"
        notificationContent.summaryArgument = "your PM"
        notificationContent.categoryIdentifier = "notifications_category"

        return notificationContent
    }
    
    private func addNotificationWithActions() {
        let props = NotificationsService.Props(id: UUID().uuidString,
                                               title: "Hi! 🐒",
                                               body: "Please, don't delete me!!!")
        let saveAction = UNNotificationAction(identifier: "accept_action",
                                              title: "Accept",
                                              options: .foreground)
        let deleteAction = UNNotificationAction(identifier: "delete_action",
                                                title: "Delete",
                                                options: .foreground)
        notificationsService.requestNotification(with: props,
                                                 actions: [saveAction, deleteAction])
    }
    
    // MARK: - IBAction
    @IBAction private func permissionsTouchedUp(_ sender: Any) {
        notificationsService.askForNotificationPermissions()
    }
    
    @IBAction private func grouppedTouchedUp(_ sender: Any) {
    }
    
    @IBAction private func categoryTouchedUp(_ sender: Any) {
        addNotificationWithActions()
    }
}

