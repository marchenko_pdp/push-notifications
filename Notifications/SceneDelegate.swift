//
//  SceneDelegate.swift
//  Notifications
//
//  Created by Nikita Marchenko on 9/23/19.
//  Copyright © 2019 Nikita Marchenko. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }
}

