//
//  NotificationsNames.swift
//  Notifications
//
//  Created by Nikita Marchenko on 05.05.2020.
//  Copyright © 2020 Nikita Marchenko. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let notificationsHasBeenAuthorized = Notification.Name("NotificationsHasBeenAuthorized")
}
